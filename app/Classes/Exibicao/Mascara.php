<?php

	namespace App\Classes\Exibicao;

	/**
	* 
	* 
	* @author Miguel Ataíde <miguelataide365@gmail.com>
	*/

	class Mascara{

		public static function aplica($val, $mask){

			//para CPFs
			if($mask === "###.###.###-##")
				$val = str_pad($val, 11, '0', STR_PAD_LEFT);
			
			$maskared = '';
			$k = 0;
			
			for($i = 0; $i <= strlen($mask)-1; $i++){
				if($mask[$i] == '#'){
					if(isset($val[$k]))
						$maskared .= $val[$k++];
					}
				else{
					if(isset($mask[$i]))
			$maskared .= $mask[$i];
				}
			}

			return $maskared;
		}

	}

?>