<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Funcionario;

class DeleteFuncionarioCommand extends Command
{
   
    private $funcionario;

    protected $signature = 'command:delete_funcionario';
    protected $description = 'Deleta um funcionário';

    /**
     * Cria uma nova instância do comando.
     *
     * @return void
     */
    public function __construct(Funcionario $funcionario)
    {
        parent::__construct();

        $this->funcionario = $funcionario;
    }

    /**
     * Executa o comando.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->funcionario->delete();
    }
}
