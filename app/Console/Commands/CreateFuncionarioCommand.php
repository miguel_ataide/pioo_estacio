<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Funcionario;
use App\Pessoa;

class CreateFuncionarioCommand extends Command
{

    private $funcionario;
    private $dados;
 
    protected $signature = 'command:create_funcionario';
    protected $description = 'Cria um funcionário';

    /**
     * Cria uma nova instância do comando.
     *
     * @return void
     */
    public function __construct(Funcionario $funcionario, $dados = array())
    {
        parent::__construct();

        $this->funcionario = $funcionario;
        $this->dados = $dados;
    }

    /**
     * Executa o comando.
     *
     * @return mixed
     */
    public function handle()
    {
        $funcionario = $this->funcionario;
        $funcionario->fill($this->dados);

        $pessoa = new Pessoa();
        $pessoa->fill($this->dados);
        $pessoa->save();

        $funcionario->id_pessoa = $pessoa->id;
        $funcionario->save();
    }
}
