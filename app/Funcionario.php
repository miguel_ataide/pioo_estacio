<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Funcionario extends Model
{
    protected $fillable = ['matricula', 'salario'];

    public function pessoa(){
        return $this->belongsTo('App\Pessoa', 'id_pessoa');
    }
}
