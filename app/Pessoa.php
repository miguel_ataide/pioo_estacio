<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pessoa extends Model
{
       protected $fillable = ['cpf', 'nome', 'email', 'telefone'];
}
