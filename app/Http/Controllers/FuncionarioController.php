<?php

namespace App\Http\Controllers;

use App\Funcionario;
use App\Console\Commands\CreateFuncionarioCommand;
use App\Console\Commands\EditFuncionarioCommand;
use App\Console\Commands\DeleteFuncionarioCommand;
use Illuminate\Http\Request;
use Illuminate\Foundation\Bus\DispatchesCommands;
use Session;

class FuncionarioController extends Controller
{
    /**
     * Exibe a lista de funcionários.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $funcionarios = Funcionario::all();

        return view('funcionarios.index')->with('funcionarios', $funcionarios);
    }

    /**
     * Mostra o formulário de criação.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('funcionarios.create');
    }

    /**
     * Armazena um novo funcionário.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'cpf' => 'required|max:11|unique:pessoas,cpf',   
            'nome' => 'required|string|max:255',
            'email' => 'required|string|email|max:255',
            'telefone' => 'required|max:11|',   
            'matricula' => 'required|max:6|unique:funcionarios,matricula,',
            'salario' => 'required|numeric',   
        ]);

        $funcionario = new Funcionario();

        $dados = array();
        $dados['cpf'] = $request->cpf;
        $dados['nome'] = $request->nome;
        $dados['email'] = $request->email;
        $dados['telefone'] = $request->telefone;
        $dados['matricula'] = $request->matricula;
        $dados['salario'] = $request->salario;

        //Chamada do comando:
        $this->dispatch(
            new CreateFuncionarioCommand($funcionario, $dados)
        );

        Session::flash('sucesso', 'Funcionário cadastrado.');

        return redirect()->route('funcionarios.index');
    }

    /**
     * Exibe o funcionário.
     *
     * @param  \App\Funcionario  $funcionario
     * @return \Illuminate\Http\Response
     */
    public function show(Funcionario $funcionario)
    {
        return view('funcionarios.show')->with('funcionario', $funcionario);
    }

    /**
     * Mostra o formulário de edição do funcionário.
     *
     * @param  \App\Funcionario  $funcionario
     * @return \Illuminate\Http\Response
     */
    public function edit(Funcionario $funcionario)
    {
        return view('funcionarios.edit')->with('funcionario', $funcionario);
    }

    /**
     * Atualiza o funcionário.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Funcionario  $funcionario
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Funcionario $funcionario)
    {    
        $request->validate([
            'cpf' => 'required|max:11|unique:pessoas,cpf,'.$funcionario->pessoa->id,   
            'nome' => 'required|string|max:255',
            'email' => 'required|string|email|max:255',
            'telefone' => 'required|max:11|',   
            'matricula' => 'required|max:6|unique:funcionarios,matricula,'.$funcionario->id,
            'salario' => 'required|numeric',   
        ]);

        $dados = array();
        $dados['cpf'] = $request->cpf;
        $dados['nome'] = $request->nome;
        $dados['email'] = $request->email;
        $dados['telefone'] = $request->telefone;
        $dados['matricula'] = $request->matricula;
        $dados['salario'] = $request->salario;

        //Chamada do comando:
        $this->dispatch(
            new EditFuncionarioCommand($funcionario, $dados)
        );

        Session::flash('sucesso', 'Funcionário editado.');

        return redirect()->route('funcionarios.index');
    }

    /**
     * Deleta o funcionário.
     *
     * @param  \App\Funcionario  $funcionario
     * @return \Illuminate\Http\Response
     */
    public function destroy(Funcionario $funcionario)
    {
        //Chamada do comando:
        $this->dispatch(
            new DeleteFuncionarioCommand($funcionario)
        );

        Session::flash('sucesso', 'Funcionário deletado.');

        return redirect()->route('funcionarios.index');
    }
}
