
  @if(Auth::check())


  <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">{{ csrf_field() }}</form>

  <li class="nav-item dropdown">
      <a class="nav-link dropdown-toggle" href="http://example.com" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        Bem-vindo {{ Auth::user()->name }}
      </a>


      <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
        <a class="nav-link" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">Sair</a>
      </div>
  </li>

      <li class="nav-item">
          <a class="nav-link" href="{{ url('/register') }}">Registrar</a>
      </li>

  
  
  
  @else

  <li class="nav-item">
      <a class="nav-link" href="{{ url('/login') }}">Login</a>
  </li>

  @endif