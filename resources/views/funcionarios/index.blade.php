@extends('principal')

@section('conteudo')
	
	<div class="container">
			
			<br><br>
			<h2> Funcionários </h2>

			<br><br>

			<div class="card">
			  	<div class="card-header">
				  	<div class="row"> 
				  		<div class="col-md-6">	
						    
				  		</div>
				  		<div class="col-md-6">
				  		  <button onclick="window.location='{{ route('funcionarios.create')}}'" type="button" data-toggle="tooltip" title = "Cadastrar Funcionário" class="btn btn-lg btn-secondary float-right"><i class="fa fa-plus"></i></button>	
				  		</div>
				  	</div>
		  		</div>

			  	<div class="card-body">
					
					<table class="table">

				  		<thead>
						    <tr>
						      <th scope="col">#</th>
							  <th scope="col">Nome</th>
							  <th scope="col">CPF</th>
							  <th scope="col">E-mail</th>
							  <th scope="col">Telefone</th>
							  <th scope="col">Matrícula</th>
							  <th scope="col">#</th>
							  <th scope="col">#</th>
							  <th scope="col">#</th>
						    </tr>
					  	</thead>
					
					  <tbody>

					  	@php
					  		$i = 1;
					  	@endphp

						@foreach($funcionarios as $funcionario)

							    <tr>
							      	
							      	<th scope="row">{{$i}}</th>
							      	<td> <p>{!! $funcionario->pessoa->nome !!}</p> </td>
							      	<td> <p>{!! $funcionario->pessoa->cpf !!}</p> </td>
							      	<td> <p>{!! $funcionario->pessoa->email !!}</p> </td>
							      	<td> <p>{!! $funcionario->pessoa->telefone !!}</p> </td>
							      	<td> <p>{!! $funcionario->matricula !!}</p> </td>
							      	
							    	<td>
										<button onclick="window.location='{{ route('funcionarios.show', $funcionario)}}'" type="button" data-toggle="tooltip" title = "Editar" class="btn silver-gray"><i class="fa fa-eye"></i></button>
									</td>

							      	<td>
										<button onclick="window.location='{{ route('funcionarios.edit', $funcionario)}}'" type="button" data-toggle="tooltip" title = "Editar" class="btn silver-gray"><i class="fa fa-edit"></i></button>
									</td>

									<td>
										<form action="{{route('funcionarios.destroy', $funcionario->id)}}" method="POST" onsubmit="return confirm('Tem certeza que deseja deletar este item?')">
						
											{{ csrf_field() }}

											<!-- needed to use DELETE method -->
											<input name="_method" type="hidden" value="DELETE">

											<button type="submit" data-toggle="tooltip" title = "Deletar" class="btn silver-gray"><i class="fa fa-trash"></i></button>
									
										</form>
									</td>

							    </tr>

					  	@php
					  		$i++;
					  	@endphp

						@endforeach

					  </tbody>
					</table>


				</div>

		</div>

	</div>

@endsection
