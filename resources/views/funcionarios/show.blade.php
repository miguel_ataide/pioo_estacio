@extends('principal')

@section('conteudo')

	<div class="container">
		
		<br>

		<h3 class="text-center"> <b>VISUALIZAR FUNCIONÁRIO</h3>
		<br>

		<div class="row">

			<div class="col-sm">

				<div class="card">
				  <div class="card-header tema-desempenho2">
				 	<b>Informações Pessoais</b>
				  </div>
				  <div class="card-body tema-desempenho">
				    
				    <h6 ><b>Nome:</b></h6>
				    <p>{{ $funcionario->pessoa->nome }}</p>

				    <h6 ><b>CPF:</b></h6>
				    <p> {{ empty($funcionario->pessoa->cpf) ? "---" : \App\Classes\Exibicao\Mascara::aplica($funcionario->pessoa->cpf, "###.###.###-##") }}</p>

				    <h6 ><b>E-mail:</b></h6>
				    <p>{{ $funcionario->user != null ? $funcionario->user->email : "---"}}</p>

				    <h6 ><b>Telefone:</b></h6>
				    <p> {{ empty($funcionario->pessoa->telefone) ? "---" : \App\Classes\Exibicao\Mascara::aplica($funcionario->pessoa->telefone, "(##) #####-####") }}</p>
				 
				  </div>
				</div>
			</div>

			<div class="col-sm">

				<div class="card">
				  <div class="card-header tema-desempenho2">
				 	<b>Informações do Funcionário</b>
				  </div>
				  <div class="card-body tema-desempenho">
				    
				    <h6 ><b>Matrícula:</b></h6>
				    <p>{{ $funcionario->matricula }}</p>

				    <h6 ><b>Salario:</b></h6>
				    <p> {{ $funcionario->salario }}</p>

				  </div>
				</div>
			</div>
		</div>

	</div>

@endsection