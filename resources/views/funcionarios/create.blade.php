@extends('principal')

@section('conteudo')

	<div class="container">

		<br>

		<h3 class="text-center"> <b>CADASTRAR FUNCIONÁRIO</h3>
		<br>

		<form action="{{route('funcionarios.store')}}" method="POST" onsubmit="doSubmit()">

			{{ csrf_field() }}

			<div class="form-row">

				<div class="form-group col-md-4">

					<label for="cpf"><b>CPF:</b></label>
		      		<input type="text" class="form-control" id="cpf" name="cpf" placeholder="Informe o CPF" value="{{old('cpf')}}" required="">

				</div>

				<div class="form-group col-md-4">
					<label for="nome"><b>Nome:</b></label>
			      	<input type="text" class="form-control" id="nome" name="nome" placeholder="Informe o nome" value="{{old('nome')}}" required="">
				</div>

				<div class="form-group col-md-4">
					<label for="nome"><b>E-mail:</b></label>
			      	<input type="email" class="form-control" id="email" name="email" placeholder="Informe o e-mail" value="{{old('email')}}" required="">
				</div>


			</div>		

			<div class="form-row">

				<div class="form-group col-md-4">
					<label for="telefone"><b>Telefone:</b></label>
			      	<input type="text" class="form-control" id="telefone" name="telefone" placeholder="Informe o telefone" value="{{old('telefone')}}" required="">
				</div>

				<div class="form-group col-md-4">
					<label for="matricula"><b>Matrícula:</b></label>
			      	<input type="matricula" class="form-control" id="matricula" name="matricula" placeholder="Informe a matrícula" value="{{old('matricula')}}" required="">
				</div>

				<div class="form-group col-md-4">

					<label for="salario"><b>Salário:</b></label>
		      		<input type="text" class="form-control" id="salario" name="salario" placeholder="Informe o salário" value="{{old('salario')}}" required="">

				</div>


			</div>		

			<br>
		 	<button type="submit" class="btn btn-lg btn-block tema-desempenho">Cadastrar</button>

		</form>

		<br><br><br>
		  
	</div>

@endsection


@section('scripts')

	<script src="/js/jquery.mask.min.js"></script>

	<script type="text/javascript">
		function doSubmit(){
			$('#cpf').unmask();
			$('#telefone').unmask();
		  	return true;
		}
	</script>

	 <script type="text/javascript">
	 	$(document).ready(function(){
  			$('#cpf').mask('999.999.999-99');
  			$('#telefone').mask('(99) 99999-9999');
		});
	 </script>

@endsection