<!DOCTYPE html>
<html lang="pt-br">

  <head>

  	@include('parciais._head')

    @yield('scripts')
    @yield('stylesheets')
  </head>
  
  <body style="font-family: verdana;">

    <div class="container">

        @include('parciais._navegacao')

        @include('parciais._mensagens')

  	    @yield('conteudo')

  	    @include('parciais._rodape')
      	    
      </div>
  </body>
</html>